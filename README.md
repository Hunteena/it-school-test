# Тестовое задание для Backend-разработчика Школы IT

## Задание 3

Реализовать RESTFull API сервис, с 3-мя эндпоинтами  
(Стек: Python, FastApi, PostgreSQL, pydantic).

1. Получение всех строк из бд.
2. Отдает данные из БД, с возможностью фильтрации по полу, с заданным LIMIT.
3. Принимает JSON{id, name, gender}, парсит данные и сохраняет в бд. Необходимо
валидировать данные (по типу).

Cтолбцы в базе данных: id, name(str), gender(str). БД заполнить случайными
данными.

### Запуск проекта

Клонировать проект
```commandline
git clone https://gitlab.com/Hunteena/it-school-test.git
```
Установить зависимости
```commandline
pip install -r requirements.txt
```
Задать переменные окружения для PostgreSQL в файле `.env` 
по аналогии с [.env.template](.env.template)

Создать контейнер с базой данных
```commandline
docker-compose up
```
Запустить приложение
```commandline
uvicorn it_school.main:app --reload
```
Swagger UI доступен по адресу [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs#/)