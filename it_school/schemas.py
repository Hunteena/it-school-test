from enum import StrEnum

from pydantic import BaseModel


class Gender(StrEnum):
    FEMALE = 'female'
    MALE = 'male'


class PersonCreate(BaseModel):
    name: str
    gender: Gender


class Person(PersonCreate):
    id: int

    class Config:
        orm_mode = True
