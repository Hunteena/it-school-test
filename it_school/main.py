from fastapi import Depends, FastAPI
from sqlalchemy.orm import Session

from . import models, schemas
from .config import settings
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/people/", response_model=list[schemas.Person])
def read_people(db: Session = Depends(get_db)):
    people = db.query(models.Person).all()
    return people


@app.get("/filter/", response_model=list[schemas.Person])
def filter_people(
        gender: schemas.Gender,
        limit: int = settings.person_limit,
        db: Session = Depends(get_db)
):
    people = db.query(models.Person).filter(
        models.Person.gender == gender
    ).limit(limit).all()
    return people


@app.post("/create/", response_model=schemas.Person)
def create_person(
        person: schemas.PersonCreate,
        db: Session = Depends(get_db)
):
    db_person = models.Person(**person.dict())
    db.add(db_person)
    db.commit()
    db.refresh(db_person)
    return db_person
